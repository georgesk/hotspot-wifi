#!/usr/bin/python3

import sys
from PyQt5 import QtWidgets
from ui_main import Ui_Form
from subprocess import call, Popen, PIPE
import time

# le nom et l'adresse IP statique de l'interface Wifi
WLAN = "wlp0s20f3"
IP   = "192.168.4.1"

class MaFenetre(QtWidgets.QWidget):
    message_on = """
<p style="font-size: 12pt;">
  L'adresse IP du service « wims-air » est : 192.168.4.1
</p>
<p style=" font-size:12pt;">
  L'accès au service wims est à l'URL : http://192.168.4.1/wims
</p>
"""
    message_off = """
<p style="font-size: 12pt;">
  Cliquez sur le bouton pour démarrer
</p>
<p style=" font-size:12pt;">
  ou arrêter le service « wims-air »
</p>
"""
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.toolButton.clicked.connect(self.bascule)
        self.uiText()
        return

    @property
    def on(self):
        return bool(self.ui.toolButton.isChecked())

    def uiText(self):
        if self.on:
            self.ui.textBrowser.setHtml(self.message_on)
        else:
            self.ui.textBrowser.setHtml(self.message_off)
        return
    
    def bascule(self):
        # dans tous les cas on arrête le wifi avant
        # toute reconfiguration
        call(f"sudo ifconfig {WLAN} down", shell=True)
        if self.on:
            # démarrage de wims-air :
            call(f"sudo ifconfig {WLAN} {IP}", shell=True)
            call(f"sudo systemctl start hostapd", shell=True)
            call(f"sudo systemctl start dnsmasq", shell=True)
        else:
            # arrêt de wims-air :
            call(f"sudo systemctl stop dnsmasq", shell=True)
            call(f"sudo systemctl stop hostapd", shell=True)
        self.uiText()
        return

if __name__ == '__main__':
    
    app = QtWidgets.QApplication(sys.argv)

    w = MaFenetre()
    w.setWindowTitle('Wims-air')
    w.show()
    
    sys.exit(app.exec_())

"""
from PyQt5 import QtCore, QtGui, QtWidgets
class RotateMe(QtWidgets.QLabel):
    def __init__(self, *args, **kwargs):
        super(RotateMe, self).__init__(*args, **kwargs)
        self._pixmap = QtGui.QPixmap()
        self._animation = QtCore.QVariantAnimation(
            self,
            startValue=0.0,
            endValue=360.0,
            duration=1000,
            valueChanged=self.on_valueChanged
        )

    def set_pixmap(self, pixmap):
        self._pixmap = pixmap
        self.setPixmap(self._pixmap)

    def start_animation(self):
        if self._animation.state() != QtCore.QAbstractAnimation.Running:
            self._animation.start()

    @QtCore.pyqtSlot(QtCore.QVariant)
    def on_valueChanged(self, value):
        t = QtGui.QTransform()
        t.rotate(value)
        self.setPixmap(self._pixmap.transformed(t))
"""
