
all: ui_main.py main_rc.py

ui_main.py: main.ui
	pyuic5 $< -o $@
main_rc.py: main.qrc
	pyrcc5 $< -o $@

clean:
	rm -rf ui_*.py *_rc.py *~ __pycache__
